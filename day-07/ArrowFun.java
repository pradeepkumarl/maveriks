import java.util.Comparator;
import java.util.*;

public class ArrowFun {
    public static void main(String[] args) {
     
        Employee ramesh = new Employee(12, "Ramesh");
        Employee kiran = new Employee(14, "Kiran");
        Employee suresh = new Employee(18, "Suresh");

        //Anonymous Inner class
        Comparator<Employee> nameDescComparator =  (emp1, emp2) -> emp2.getName().compareTo(emp1.getName());
                    
        
        Set<Employee> employees = new TreeSet<>(nameDescComparator);
        employees.add(ramesh);
        employees.add(kiran);
        employees.add(suresh);

        System.out.println(employees);
    }
}

class NameSortComparator implements Comparator<Employee>{

    public int compare(Employee employee1, Employee employee2){
        return employee1.getName().compareTo(employee2.getName());
    }
}

class NameDescSortComparator implements Comparator<Employee>{

    public int compare(Employee employee1, Employee employee2){
        return employee2.getName().compareTo(employee1.getName());
    }
}

class Employee implements Comparable<Employee> {
    private long id;
    private String name;

    public String getName(){
        return this.name;
    }
    public Employee( long id, String name){
        this.id = id;
        this.name = name;
    }

    
    public int compareTo(Employee emp){
        return (int)(this.id - emp.id);
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", name=" + name + "]";
    }


}