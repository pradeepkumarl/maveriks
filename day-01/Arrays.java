public class Arrays {
    public static void main(String args[]){
        /*1. Arrays is fixed data type 
          2. Fixed size and the size has be specified upfront 
          3. The memory allocation will be contigous
          4. The default values will be populated based on the data type
                numbers [int, byte, short, double and long ]- 0
                boolean = false 
                char = ""
                objects = null 
        */
        int[] numbers = new int[10];
        numbers[2] = 90;
        numbers[9] = 90;
        int[] values = { 11, 22, 33, 44, 55, 66 };

        System.out.println(values[2]);

        //iterate over an array 
        for(int index = 0; index < numbers.length; index ++){
            System.out.println("The number at numbers ["+index+"]" +"is "+ numbers[index]);
        }
        
              
    }
}