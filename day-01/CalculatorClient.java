class Calculator {

    public int sum(int operand1, int operand2){
        return operand1 + operand2;
    }
    public int sub(int operand1, int operand2){
        return operand1 - operand2;
    }
    public int product(int operand1, int operand2){
        return operand1 * operand2;
    }
}

public class CalculatorClient {

    public static void main(String[] args) {
        // datatype variable  = value;
        Calculator cal = new Calculator();
        int result = cal.sum(34,34);
        System.out.println("The result is "+ result);
    }

}