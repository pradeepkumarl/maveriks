/*
   int[][] twoDim = new int[4][4]
   int initData = 10;
              col
        row-> 10 11 12 13
            14 15 16 17
            18 19 20 21
            22 23 24 25
*/

public class TwoDimArray {
    static int size = 4;
    static int [][] matrix = new int[size][size];
    static int initData = 10;
    
    public static void main(String args[]){
       //populate the matrix
        populateMatrix();

        //print the elements in the matrix format
        printMatrix();
    }

    static void populateMatrix(){
        for ( int row = 0; row < size; row ++){
            for ( int col = 0; col < size; col ++) {
                matrix[row][col] = initData ++;
            }
        }
    }

    static void printMatrix(){
        for ( int row = 0; row < size; row ++){
            for ( int col = 0; col < size; col ++) {
                System.out.print("\t "+matrix[row][col]+ "\t");
            }
            System.out.println();
        }
    }
}