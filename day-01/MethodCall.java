public class MethodCall{
    public static void main(String[] args) {
        System.out.println("Before calling method1");
        method1();
        System.out.println("After calling method1");
    }

    static void method1(){
        System.out.println("Before calling method2");
        method2();
        System.out.println("After calling method2");
    }

    static void method2(){
        System.out.println("Before calling method3");
        method3();
        System.out.println("After calling method3");
    }

    static void method3(){
        System.out.println("Executing method 3");
    }
}