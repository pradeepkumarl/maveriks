public class ControlStructures {
    public static void main(String[] args) {
        boolean flag = false;
        if (flag){
            System.out.println("Value is false");
        } else {
            System.out.println("Value is true");
        }
        String dayOfTheWeek = "Wednesday";
        if (dayOfTheWeek == "Sunday" || dayOfTheWeek == "Saturday"){
            System.out.println("Its a Weekend !");
        } else if (dayOfTheWeek == "Monday"){
            System.out.println("Beginning of the week ");
        } else if (dayOfTheWeek == "Tuesday"){
            System.out.println("Settling down");
        } else if (dayOfTheWeek == "Wednesday"){
            System.out.println("Balanced week");
        } else {
            System.out.println("Preparing for Weekend !!");
        }
        //switch case
        switch (dayOfTheWeek) {
            case "Wednesday":
                    System.out.println("This switch case execute");
                    break;
            default:
                System.out.println("This is a defualt case.");
                break;
        }
    }
}