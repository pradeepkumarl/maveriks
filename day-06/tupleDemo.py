subjects=("maths", "science", "zoology", "chemistry")

#print(subjects[1])

#tuples are immutable. to add values, convert to list first and 
# then create a tuple from the list

values = list(subjects)
#modify the list
values.append("botany")
values.remove("science")
subjects = tuple(values)
print(subjects)


#destructuring

players=("Dhoni", 'kohli', 'Raina', 'Harbajan')

(captain, vCaptain, *rest) = players

#print(captain)
#print(vCaptain)
#print(*rest)

# loop through the tuple
#for player in players:
#    print(player)

name = " hexaware"
print(name[1:].upper().lower())
