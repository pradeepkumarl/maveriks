import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/*
 * HasMap and TreeMap are implementation
 */
public class HashMapDemo {
	
	public static void main(String[] args) {
		Map<Integer, Item> map = new HashMap<>();
		
		map.put(11,  new Item(11, "Pen", 33));
		map.put(12,  new Item(11, "Pen", 33));
		map.put(11,  new Item(11, "Pen", 33));
		map.put(14,  new Item(12, "Eraser", 10));
		map.put(15,  new Item(11, "Pen", 33));
		
		System.out.println("The size of the map is "+ map.size());
		
		Set<Integer> keys = map.keySet();
		Collection<Item> values = map.values();
		/*
		Iterator<Integer> keysIt = keys.iterator();
		
		while(keysIt.hasNext()) {
			System.out.println("Value is "+ map.get(keysIt.next()));
		}
		*/
		Iterator<Integer> keysIt = keys.iterator();
		
		//System.out.println(values);
		
		Set<Entry<Integer, Item>> entrySet = map.entrySet();
		
		Iterator<Entry<Integer, Item>> it = entrySet.iterator();
		
		while(it.hasNext()) {
			Entry<Integer, Item> entry = it.next();
			System.out.println("Key is "+ entry.getKey()+ " value is : "+ entry.getValue());
		}
		
		
		
	}

}
