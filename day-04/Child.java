public class Child extends Parent {
    
    public void method() throws CustomException{

    }
}

class CustomException extends Exception {

    public CustomException(String message){
        super(message);
    }

    @Override
    public String getMessage(){
        return super.getMessage();
    }

}
