import java.io.File;
import java.io.FileNotFoundException;
import java.io.*;

public class CheckedExceptionDemo {

    public static void main(String[] args) {
        try {
            processFile("D://Notes.md");
        } catch(Exception e) {
            System.out.println(" Error while reading the contents of the file ....");
        }
    }

    private static void processFile(String fileName) throws FileNotFoundException, IOException {
        File file = new File(fileName);
        FileReader reader = new FileReader(file);
        char c = (char)reader.read();
        System.out.println("Character :: "+ c);
    }
}
