public class BankAccount {

    private double accountBalance; 
    private String accountName;
    private String accountNumber;

  
    public BankAccount(String accoutName, double initialDeposit){
        this.accountName = accoutName;
        this.accountBalance = initialDeposit;
    }


    public void deposit(double amount){
        this.accountBalance += amount;
    }
    /*
      In current account, the min balace should be 35K
      In savings account, the min balance should be 50K
      In salarid account, the min balance can be 0
    */
    public double withdraw(double amount){
        if ( this.accountBalance >= amount){
            this.accountBalance -=amount;
            return amount;
        }
        return 0;
    }

    public double getBalance(){
        return this.accountBalance;
    }
}


