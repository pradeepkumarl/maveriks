public class CurrentAccount  extends BankAccount {

    private String ifscCode;
    
    public CurrentAccount(String custName, double initalBal){
        super(custName, initalBal);
    }

    public CurrentAccount(String custName){
        super(custName, 0);
    }

    public CurrentAccount(String cusName, String ifscCode){
        super(cusName, 0);
        this.ifscCode = ifscCode;
    }

    @Override
    public void deposit(double amount){
        //custom implementation
        if (amount > 20_00_000){
            System.out.println(" You cannot deposit more than 2000000 Rs at one time");
        }else {
            super.deposit(amount);
        }
    }
}
