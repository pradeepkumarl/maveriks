package com.classpath.javatrack.pack1;

public class PackageA {
    
    private int privateVariable = 11;
    protected int protectedVariable = 22;
    int defaultVariable = 33;
    public int publicVariable = 44;
}
