package com.classpath.collections;

import java.util.List;
import java.util.ArrayList;

public class ArrayListDemo {
	
	public static void main(String[] args) {
		
		List<Integer> numbers = new ArrayList<>();
		numbers.add(34);
		numbers.add(22);
		numbers.add(45);
		numbers.add(31);
		
		int [] a = new int[10];
		
		List<String> names = new ArrayList<>();
		names.add("Kavitha");
		names.add("Rahima");
		names.add("Keerthi");
		names.add("VishnuPriya");
		
		System.out.println(names.contains("Kavitha"));
		
		System.out.println("INdex of Kavitatha is "+ names.indexOf("Kavitha"));
		
		
		System.out.println("Number at index 1 is "+numbers.get(1));
		System.out.println("Number at index 5 is "+numbers.get(3));
		
		System.out.println("Size of the list is "+ numbers.size());
		
		System.out.println("List containes 32 " +numbers.contains(32));
		System.out.println("List containes 34 " +numbers.contains(34));
		
		numbers.clear();
		
		System.out.println("Size of the numbers array after clearing "+ numbers.size());
		
	}

}
