package com.classpath.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderDemo {
	
	public static void main(String[] args) {
		
		File file = new File("D://data.txt");
		
		//try with resources
		try (BufferedReader reader = new BufferedReader(new FileReader(file));){
			
			boolean  flag = true;
			
			while(flag) {
				String value = reader.readLine();
				if (value == null) {
					flag = false;
					continue;
				}
				System.out.println(value);
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("File is not present..");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

}
