package com.classpath.javatrack.pack2;

import com.classpath.javatrack.pack1.*;

public class PackageA extends PackageB{

    public static void main(String[] args) {
        //current class
        PackageA objA = new PackageA();
        //fully qualified class name 
//        com.classpath.javatrack.pack1.PackageA  pack2ObjA = new com.classpath.javatrack.pack1.PackageA();
        PackageB objB = new PackageB();

        //System.out.println("private variable "+ objB.privateVariable);
        System.out.println("protected variable "+ objA.protectedVariable);
        //System.out.println("default variable "+ objB.defaultVariable);
        System.out.println("public variable "+ objB.publicVariable);

    }
}
