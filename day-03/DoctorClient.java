public class DoctorClient {

    public static void main(String[] args) {
        //Doctor doctor = new Doctor(12, "Suresh");
        Doctor ortho = new Dentist(12, "Suresh");

        ortho.treatPatient();
        ortho.chargeConsultationFee();
        
    }
    
}
