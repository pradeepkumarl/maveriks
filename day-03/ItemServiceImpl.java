public class ItemServiceImpl implements ItemService {

    private Item[] items = new Item[10];
    private static int counter = 0;

    public void saveItem(Item item){
        if ( counter <= 9){
            items[counter] = item;
            counter++;    
        }
    }

    public Item[] fetchItems(){
        return this.items;
    }

    public Item findItemById(long itemId) throws ItemNotFoundException {
        for (int index = 0; index < 10; index ++){

            if (items[index] != null && items[index].getItemId() == itemId){
                return items[index];
            }
        }
        throw new ItemNotFoundException("Item with the given id is not present");
    }

    public void deleteItemById(long itemId){
        for (int index = 0; index < 10; index ++){
            if (items[index].getItemId() == itemId){
                items[index] = null;
            }
        }
    }
}
