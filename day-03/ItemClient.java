import java.io.IOException;
import java.util.Scanner;

public class ItemClient {

    public static void main(String[] args) {

        //accept the input 
        //1 - save, 2 list , 3 fetch Item by id, 4 delete 
        
        Item item = new Item(12, "MacBookpro", 2_00_000);
        Item iphone = new Item(13, "IPhone", 88_000);

        ItemService itemService = new ItemServiceImpl();
        itemService.saveItem(item);
        itemService.saveItem(iphone);

        //fetch items 
        Item[] allItems = itemService.fetchItems();
        for (Item i: allItems){
            if (i != null){
                System.out.println("Item :: "+ i.getName());
            }
        }

        //fetch Item by Id 
        try {
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println(" Please enter item Id ");
        int input = sc.nextInt();
            Item returnedItem = itemService.findItemById(input);
            if (returnedItem != null){
                System.out.println("Fetched Item by id : "+ returnedItem.getName());
            }
        } catch(ItemNotFoundException){
            //System.out.println("Item with the given id is not present");
            System.out.println(exception.getMessage());
        } finally {
            System.out.println(" This code will be executed irrespective of the exception is thrown or not");
            //Use the finally block to clean up the resources
            sc.close();
        }
    }
}
