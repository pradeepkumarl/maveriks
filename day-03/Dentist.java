public class Dentist extends Doctor{
    public Dentist(int exp, String name ){
        super(exp, name);
    }

    @Override
    public final void treatPatient(){
        System.out.println("Conducting Root Canal for Patient....");
    }
}
