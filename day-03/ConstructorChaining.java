public class ConstructorChaining extends Parent {


    public ConstructorChaining(){
        super("default message");
        System.out.println("Called the default no args constructor....");
    }
    
    public ConstructorChaining(String message){
        super(message);
        System.out.println("Called the constructor with args .... "+ message);
    }
    
    public void test(){
        System.out.println("Excecuted the test method");
    }

    public static void main(String[] args) {
        ConstructorChaining obj = new ConstructorChaining("hello world");
        //obj.test();
    }
}

class Parent extends Object {

   

    public Parent(String message){
        System.out.println(" Called inside the constructor with args inside the Parent");
    }
}
