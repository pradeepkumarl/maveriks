public class PrintObject {
    
    public static void main(String[] args) {
        System.out.println(" integer "+ 1);
        System.out.println(" boolean "+ false);
        System.out.println(" String "+ "test");
        System.out.println(" float "+ 1200240.24);

        Item item = new Item(1234, "IPhone", 45_000);

        System.out.println("Item "+ item);
    }
}
